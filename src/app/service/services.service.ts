import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  endpointBombos: string;
  endpointGroups: string;

  constructor(private http: HttpClient) {
    this.endpointBombos = '../../assets/data-json/dataBombos.json'
    this.endpointGroups = '../../assets/data-json/dataGroups.json'
  }

  // tslint:disable-next-line:typedef
  handleError(error: HttpErrorResponse){
    return throwError(error);
  }

  private setHeaders(): HttpHeaders {
    const headersConfig = {
        'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
        'Content-Type': 'application/json',
        Accept: 'application/json',
    };
    return new HttpHeaders(headersConfig);
  }

  getServiceBombo(): Observable<any> {
    const endpoint = this.endpointBombos
    return this.http.get(endpoint , {responseType: 'text'}).pipe(
      map(
        (data: any) => {
          const dateGroup = data;
          return JSON.parse(dateGroup);
        }
      ),
      catchError(this.handleError)
    );
  }

  getServiceGroup(): Observable<any> {
    const endpoint = this.endpointGroups
    return this.http.get(endpoint , {responseType: 'text'}).pipe(
      map(
        (data: any) => {
          const dateGroup = data;
          return JSON.parse(dateGroup);
        }
      )
    )
  }
}
