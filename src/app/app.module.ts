import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ServicesService } from './service/services.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BombosComponent } from './components/bombos/bombos.component';
import { GroupsComponent } from './components/groups/groups.component';

@NgModule({
  declarations: [
    AppComponent,
    BombosComponent,
    GroupsComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],

  providers: [ServicesService, HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
