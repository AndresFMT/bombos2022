import { Component, OnInit, Input } from '@angular/core';
import { ServicesService } from '../../service/services.service';
@Component({
  selector: 'app-bombos',
  templateUrl: './bombos.component.html',
  styleUrls: ['./bombos.component.less']
})
export class BombosComponent implements OnInit {

  index: any;
  dataValues: any[] = [];

  constructor(private _services: ServicesService) { }

  ngOnInit(): void {
    this.dataLoad();
  }

  dataLoad(): void {

    let dataKeys = []; //For keys

    this._services.getServiceBombo().subscribe(
      data => {
        let info = data;
        for (let key in info) {
          this.dataValues.push(info[key]);
          this.index = dataKeys.push(key);
          console.log(this.index);
          if (Object.prototype.hasOwnProperty.call(data, key)) {
            const element = data[key];
            for (const iterator of element) {
              // console.log(iterator);
            }
          }
        }
      },
      err => {
        console.log(err);
        return err;
      }
    );
  }

  // getTeamList(): void {
  //   this.service.getTeamList().subscribe((teams: Team[]) => {
  //     for (const key in teams) {
  //       if (teams.hasOwnProperty(key)) {
  //         this.teams.push(teams[key]);
  //         this.keys.push(key);
  //       }
  //     }
  //   });
  // }
}
