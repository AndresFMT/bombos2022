import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BombosComponent } from './bombos.component';

describe('BombosComponent', () => {
  let component: BombosComponent;
  let fixture: ComponentFixture<BombosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BombosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BombosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
